defmodule BlogWeb.Acceptance.ShowPostTest do
  use Blog.DataCase
  use Hound.Helpers

  hound_session()

  setup do
    alias Blog.Content
    alias Blog.Content.Post
  end

  test "presence of a specific post" do
    navigate_to("/post/1")

    post = find_element(:css, ".blog_preview")
    title = find_within_element(post, :css, ".title") |> visible_text()
    body = find_within_element(post, :css, ".body") |> visible_text()


    assert title == "First Post"
    assert body == "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC"

  end

end
