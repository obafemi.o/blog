defmodule BlogWeb.Acceptance.HomepageTest do
  use Blog.DataCase
  use Hound.Helpers

  hound_session()

  setup do
    alias Blog.Content
    alias Blog.Content.Post

    post = %Post{
      id: 1,
      title: "Post Test",
      body: "Post body"
    }
    :ok
  end

  test "presence of blog posts on homepage" do
    navigate_to("/")

    post = find_element(:css, ".blog_list")
    title = find_within_element(post, :css, ".title") |> visible_text()
    body = find_within_element(post, :css, ".body") |> visible_text()


    assert title == "Post Test"
    assert body == "Post body"

  end

end
