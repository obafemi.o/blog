defmodule BlogWeb.PageController do
  use BlogWeb, :controller

  alias Blog.Content
  alias Blog.Content.{Post, Comment}

  def index(conn, _params) do
    posts = Content.list_posts()
    render(conn, "index.html", posts: posts)
  end
end
