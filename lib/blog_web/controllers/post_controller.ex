defmodule BlogWeb.PostController do
  use BlogWeb, :controller

  alias Blog.Content
  alias Blog.Content.{Post, Comment, Tag}

  def index(conn, _params) do
    posts = Content.list_posts()
    render(conn, "index.html", posts: posts)
  end

  def new(conn, _params) do
    changeset = Content.change_post(%Post{})
    tags = Content.list_tags()
    render(conn, "new.html", changeset: changeset, tags: tags)
  end

  def create(conn, %{"post" => post_params}) do
    case Content.create_post(post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post created successfully.")
        |> redirect(to: Routes.post_path(conn, :show, post))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    post = Content.get_post!(id)
    comment_changeset = Content.change_comment(%Comment{})
    tag_changeset = Content.change_tag(%Tag{})
    tags = Content.list_tags()
    render(conn, "show.html", post: post, comment_changeset: comment_changeset, tag_changeset: tag_changeset, tags: tags)
  end

  def edit(conn, %{"id" => id}) do
    post = Content.get_post!(id)
    changeset = Content.change_post(post)
    render(conn, "edit.html", post: post, changeset: changeset)
  end

  def update(conn, %{"id" => id, "post" => post_params}) do
    post = Content.get_post!(id)

    case Content.update_post(post, post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post updated successfully.")
        |> redirect(to: Routes.post_path(conn, :show, post))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", post: post, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    post = Content.get_post!(id)
    {:ok, _post} = Content.delete_post(post)

    conn
    |> put_flash(:info, "Post deleted successfully.")
    |> redirect(to: Routes.post_path(conn, :index))
  end

  def add(conn, _params) do
    tags = Content.list_tags
    render(conn, "add.html",tags: tags)
  end

  def tag(conn, %{ "post" => post_params}) do
    post_id = Map.get(post_params, "post_id")
    tag_id = Map.get(post_params, "tag_id")


    case Content.add_tag(post_id, tag_id) do
      {:ok,_} ->
        conn
        |> put_flash(:info, "Tags added successfully.")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:info, "Memo already  sent to this user.")
        render conn, "new.html"
    end
  end
end
