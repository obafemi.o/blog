defmodule BlogWeb.SessionController do
  use BlogWeb, :controller

  alias Blog.Account

  def new(conn, _params) do
    render(conn, "new.html")
  end

  # def create(conn, %{"session" => auth_params}) do
  #   user = Account.get_by_email(auth_params["email"])
  #   case Comeonin.Bcrypt.verify_pass(user, auth_params["password"]) do
  #   {:ok, user} ->
  #     conn
  #     |> put_session(:current_user_id, user.id)
  #     |> put_flash(:info, "Signed in successfully.")
  #     |> redirect(to: Routes.page_path(conn, :index))
  #   {:error, _} ->
  #     conn
  #     |> put_flash(:error, "There was a problem with your username/password")
  #     |> render("new.html")
  #   end
  # end

  def create(conn, %{"session" => session_params}) do
    case Account.get_user_by_credentials(session_params) do
      :error ->
        conn
        |> put_flash(:error, "Invalid username/password combination")
        |> render("new.html")
      user ->
        conn
        |> assign(:current_user, user)
        |> put_session(:user_id, user.id)
        |> configure_session(renew: true)
        |> put_flash(:info, "Login successful")
        |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def delete(conn, _params) do
    conn
    |> delete_session(:current_user_id)
    |> put_flash(:info, "Signed out successfully.")
    |> redirect(to: Routes.page_path(conn, :index))
  end
end
