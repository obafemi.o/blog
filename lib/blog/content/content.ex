defmodule Blog.Content do
  @moduledoc """
  The Content context.
  """

  import Ecto.Query, warn: false
  alias Blog.Repo

  alias Blog.Content.Post

  @doc """
  Returns the list of posts.

  ## Examples

      iex> list_posts()
      [%Post{}, ...]

  """
  def list_posts do
    Repo.all(Post)
  end

  @doc """
  Gets a single post.

  Raises `Ecto.NoResultsError` if the Post does not exist.

  ## Examples

      iex> get_post!(123)
      %Post{}

      iex> get_post!(456)
      ** (Ecto.NoResultsError)

  """
  def get_post!(id) do
    Post
    |> Repo.get!(id)
    |> Repo.preload(:comments)
    |> Repo.preload(:tags)
  end


  @doc """
  Creates a post.

  ## Examples

      iex> create_post(%{field: value})
      {:ok, %Post{}}
def add_tag(pid, %Tag{} = tag) do
  #   PostTag.changeset(%PostTag{}, %{post_id: pid, tag_id: tag.tag_id})
  #   |> Repo.insert()
  # end
      iex> create_post(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_post(attrs \\ %{}) do
    %Post{}
    |> Post.changeset(attrs)
    |> Repo.insert()
  end


  @doc """
  Updates a post.

  ## Examples
def add_tag(pid, %Tag{} = tag) do
  #   PostTag.changeset(%PostTag{}, %{post_id: pid, tag_id: tag.tag_id})
  #   |> Repo.insert()
  # end
      iex> update_post(post, %{field: new_value})
      {:ok, %Post{}}

      iex> update_post(post, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_post(%Post{} = post, attrs) do
    post
    |> Post.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Post.

  ## Examples

      iex> delete_post(post)
      {:ok, %Post{}}

      iex> delete_post(post)
      {:error, %Ecto.Changeset{}}

  """
  def delete_post(%Post{} = post) do
    Repo.delete(post)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking post changes.

  ## Examples

      iex> change_post(post)
      %Ecto.Changeset{source: %Post{}}

  """
  def change_post(%Post{} = post) do
    Post.changeset(post, %{})
  end

  alias Blog.Content.Comment

  @doc """
  # def add_tag(pid, %Tag{} = tag) do
  #   PostTag.changeset(%PostTag{}, %{post_id: pid, tag_id: tag.tag_id})
  #   |> Repo.insert()
  # end
  Returns the list of comments.

  ## Examples

      iex> list_comments()
      [%Comment{}, ...]

  """
  def list_comments do
    Repo.all(Comment)
  end

  @doc """
  Gets a single comment.
  def add_tag(pid, %Tag{} = tag) do
  #   PostTag.changeset(%PostTag{}, %{post_id: pid, tag_id: tag.tag_id})
  #   |> Repo.insert()
  # end
  Raises `Ecto.NoResultsError` if the Comment does not exist.

  ## Examples

      iex> get_comment!(123)
      %Comment{}

      iex> get_comment!(456)
      ** (Ecto.NoResultsError)

  """
  def get_comment!(id), do: Repo.get!(Comment, id)

  @doc """
  Creates a comment.def add_tag(pid, %Tag{} = tag) do
  #   PostTag.changeset(%PostTag{}, %{post_id: pid, tag_id: tag.tag_id})
  #   |> Repo.insert()
  # end

  ## Examples

      iex> create_comment(%{field: value})
      {:ok, %Comment{}}

      iex> create_comment(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_comment(%Post{} = post, attrs \\ %{}) do
    post
    |> Ecto.build_assoc(:comments)
    |> Comment.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a comment.

  ## Examples

      iex> update_comment(comment, %{field: new_value})
      {:ok, %Comment{}}

      iex> update_comment(comment, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_comment(%Comment{} = comment, attrs) do
    comment
    |> Comment.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Comment.

  ## Examples

      iex> delete_comment(comment)
      {:ok, %Comment{}}

      iex> delete_comment(comment)
      {:error, %Ecto.Changeset{}}

  """
  def delete_comment(%Comment{} = comment) do
    Repo.delete(comment)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking comment changes.

  ## Examples

      iex> change_comment(comment)
      %Ecto.Changeset{source: %Comment{}}

  """
  def change_comment(%Comment{} = comment) do
    Comment.changeset(comment, %{})
  end

  alias Blog.Content.Tag

  def list_tags do
      Repo.all(Tag)
  end

  def create_tag(attrs \\ %{}) do
    %Tag{}
    |> Tag.changeset(attrs)
    |> Repo.insert()
  end

  def change_tag(%Tag{} = tag) do
    Tag.changeset(tag, %{})
  end

  alias Blog.Content.PostTag

  def put_tag(%Post{} = post, %Tag{} = tag) do
    post
    |> Repo.preload(:tags) # Load existing data
    |> Ecto.Changeset.change() # Build the changeset
    |> Ecto.Changeset.put_assoc(:tags, [tag]) # Set the association
    |> Repo.update!
  end

  def add_tag(pid, %Tag{} = tag) do
    PostTag.changeset(%PostTag{}, %{post_id: pid, tag_id: tag.tag_id})
    |> Repo.insert()
  end

  def add_tag(pid, tid) do
    PostTag.changeset(%PostTag{}, %{post_id: pid, tag_id: tid})
    |> Repo.insert()
  end

  # def upsert_post_tags(post, tag_ids) when is_list(tag_ids) do
  #   tags =
  #     Tag
  #     |> where([tag], tag.id in ^tag_ids)
  #     |> Repo.all()

  #   with {:ok, _struct} <-
  #          post
  #          |> Post.changeset_update_tags(tags)
  #          |> Repo.update() do
  #     {:ok, Content.get_post(post.id)}
  #   else
  #     error ->
  #       error
  #   end
  # end

  def get_tag!(id), do: Repo.get!(Tag, id)

  def delete_tag(%Tag{} = tag) do
    Repo.delete(tag)
  end

  def update_tag(%Tag{} = tag, attrs) do
    tag
    |> Tag.changeset(attrs)
    |> Repo.update()
  end

end
