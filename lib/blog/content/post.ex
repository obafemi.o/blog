defmodule Blog.Content.Post do
  use Ecto.Schema
  import Ecto.Changeset

  alias Blog.Content.Tag

  schema "posts" do
    field :body, :string
    field :title, :string
    has_many :comments, Blog.Content.Comment
    many_to_many(
      :tags,
      Tag,
      join_through: "post_tags",
      on_replace: :delete
    )

    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:title, :body])
    |> validate_required([:title, :body])
  end
end
