defmodule Blog.Content.PostTag do
  use Ecto.Schema
  import Ecto.Changeset

  alias Blog.Content.{Post, Tag}
  alias Blog.Content

  schema "post_tags" do
    belongs_to :post, Post
    belongs_to :tag, Tag

    timestamps()
  end

  @doc false
  def changeset(post_tag, attrs) do
    post_tag
    |> cast(attrs, [:post_id, :memo_id])
    |> validate_required([])
  end
end
